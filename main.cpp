// Реализация односвязного списка

#include <iostream>

using std::cout;
using std::cin;
using std::endl;

template <typename T>
class List
{
	private:
	
	struct Element
	{
		T date;
		Element* next;
	} *first;
	
	int count;	// Количество элементов в списке
	
	public:
	
	// Узнать количество элементов в списке
	int getCount()
	{
		return this->count;
	}

	//Добавить элемент в конец списка
	void AddElementToEnd(T date)
	{
		Element* tmp = this->first;
		while (tmp->next != nullptr)
		{
			tmp = tmp->next;
		}
		
		Element* adde = new Element;	// Создали новый элемент
		adde->date = date;
		adde->next = nullptr;	// Теперь этот элемент последний
		tmp->next = adde;	// Связали последний элемент с остальным списком
		
		this->count++;
	}
	
	// Напечатать все элементы
	void print()
	{
		Element* tmp = this->first;
	
		while (tmp->next != nullptr)
		{
			cout << tmp->date << endl;
			tmp = tmp->next;
		}
		cout << tmp->date << endl;
	}
	
	// Конструктор
	List(T date)
	{
		Element* tmp = new Element;
	
		tmp->date = date;
		tmp->next = nullptr;
		this->first = tmp;
	
		this->count = 1;
	};
	
	// Деструктор
	~List()
	{
		if (this->first != nullptr)
		{
			Element* tmp = this->first;
	
			while (this->first->next != nullptr)	// Пока первый элемент не единственный
			{
				tmp = this->first;
				this->first = this->first->next;	// Первым элементом теперь является следующий элемент
				delete (tmp);	// Удаляем бывший первый элемент
				this->count--;
			}
			
			delete (this->first);
			this->count--;
			
		}
	};
};

int main()
{	
	List<int> list(10);
	
	list.AddElementToEnd(11);
	list.AddElementToEnd(12);
	list.AddElementToEnd(13);
	list.AddElementToEnd(14);
	list.AddElementToEnd(15);
	
	list.print();
	
	cout << "Count elements in list: " << list.getCount() << endl;
	
	return 0;
}